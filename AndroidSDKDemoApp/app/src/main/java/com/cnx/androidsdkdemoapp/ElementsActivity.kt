package com.cnx.androidsdkdemoapp

import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.cnx.androidsdkdemoapp.databinding.ActivityElementsBinding
import com.cnx.connatixplayersdk.external.ElementsPlayer
import com.cnx.connatixplayersdk.external.interfaces.*
import com.cnx.connatixplayersdk.external.models.*

private const val TAG = "ElementsActivity"

class ElementsActivity : AppCompatActivity(), ConnatixPlayerListener {

    private lateinit var connatixPlayer: ElementsPlayer
    private lateinit var binding: ActivityElementsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityElementsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        setupPlayer()
        setupRecyclerView()
    }

    private fun setupPlayer() {

        /*
        Player init
        Optionally, you can also define ElementsSettings and ElementsCustomization objects here
        */
        val appSettings = AppSettings(
            domainURL = "https://example.com",
            storeURL = "https://playstore.com/example",
            appCategories = listOf("news"),
            hasPrivacyPolicy = true,
            isPaid = false,
            appPageURL = "https://example.com/web-equivalent-article-page",
            useContentOnlyDomain = true
        )

        val elementsConfig = ElementsConfig(
            playerId = "a1c25364-9ad9-4636-a194-7886a24aedf7",
            customerId = "51a004ea-0018-4342-83c4-9a983a0ee702",
            appSettings = appSettings
        )

        connatixPlayer = ElementsPlayer(this, elementsConfig, this)

        // Add player event listeners
        connatixPlayer.listenFor(EventType.PAUSE)
        connatixPlayer.listenFor(EventType.PLAY)
        connatixPlayer.listenFor(EventType.VOLUME_CHANGED)

        // Or listen to all events
        connatixPlayer.listenForAllEvents()

        // Start rendering the player
        connatixPlayer.start()

        // Add player to layout
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        connatixPlayer.layoutParams = params
        binding.elementsContainer.addView(connatixPlayer.view)
    }

    private fun setupRecyclerView() {

        val listItems = arrayOfNulls<Int>(10)
        val imageRecyclerViewAdapter = RecyclerViewAdapter(listItems)
        binding.imageRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.imageRecyclerView.adapter = imageRecyclerViewAdapter
    }

    override fun onPause() {

        connatixPlayer.prepareForOnPause()
        super.onPause()
    }

    override fun onResume() {

        connatixPlayer.prepareForOnResume()
        super.onResume()
    }

    override fun onDestroy() {

        connatixPlayer.stop()
        super.onDestroy()
    }

    override fun receivedConnatixEvent(event: PlayerEvent) {
        Log.d(TAG, "🔴 Event type: ${event.type.value}, payload: ${event.payload}")
    }
}