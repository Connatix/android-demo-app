package com.cnx.androidsdkdemoapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.cnx.androidsdkdemoapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        setupButtons()
    }

    private fun setupButtons() {

        binding.elementsButton.setOnClickListener {
            val intent = Intent(this, ElementsActivity::class.java)
            startActivity(intent)
        }

        binding.playspaceButton.setOnClickListener {
            val intent = Intent(this, PlayspaceActivity::class.java)
            startActivity(intent)
        }

        binding.gamIntegrationButton.setOnClickListener {
            val intent = Intent(this, GAMActivity::class.java)
            startActivity(intent)
        }
    }
}
