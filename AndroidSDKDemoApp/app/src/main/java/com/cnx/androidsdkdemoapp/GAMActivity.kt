package com.cnx.androidsdkdemoapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.cnx.androidsdkdemoapp.databinding.ActivityGamBinding
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView

class GAMActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGamBinding

    private val adUnitId = "/22034550994/1029384756"
    private val adSize = AdSize(300, 250)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGamBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        loadAd(adUnitId, adSize)
    }

    private fun loadAd(adUnitId: String, adSize: AdSize) {

        val adView = AdManagerAdView(this)
        binding.containerView.addView(adView)
        val layoutParams = adView.layoutParams as ConstraintLayout.LayoutParams

        layoutParams.topToTop = 0
        layoutParams.bottomToBottom = 0
        layoutParams.startToStart = 0
        layoutParams.endToEnd = 0
        layoutParams.horizontalBias = 0.5F
        layoutParams.verticalBias = 0.5F
        adView.layoutParams = layoutParams

        adView.adUnitId = adUnitId
        adView.setAdSize(adSize)

        // Add values for the macros defined in the grab code.
        // This grab code can be found on the Connatix console.
        // The values below are examples, you should provide real ones instead
        val adRequest = AdManagerAdRequest.Builder()
            .addCustomTargeting("appPageURL", "https://example.com/web-equivalent-article-page")
            .addCustomTargeting("us_privacy_string", "")
            .addCustomTargeting("gdpr_parsed_vendor_string", "")
            .addCustomTargeting("gdpr_consent", "")
            .build()
        adView.loadAd(adRequest)
    }
}