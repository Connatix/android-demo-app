package com.cnx.androidsdkdemoapp

import androidx.recyclerview.widget.RecyclerView
import com.cnx.androidsdkdemoapp.databinding.ListPlaceholderCellBinding

class ImageViewHolder(private val binding: ListPlaceholderCellBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind() {
        binding.placeholderImageView.setImageResource(R.drawable.list_placeholder_image)
    }
}