package com.cnx.androidsdkdemoapp

import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.cnx.androidsdkdemoapp.databinding.ActivityPlayspaceBinding
import com.cnx.connatixplayersdk.external.PlayspacePlayer
import com.cnx.connatixplayersdk.external.interfaces.*
import com.cnx.connatixplayersdk.external.models.*

private const val TAG = "PlayspaceActivity"

class PlayspaceActivity : AppCompatActivity(), ConnatixPlayerListener {

    private lateinit var connatixPlayer: PlayspacePlayer
    private lateinit var binding: ActivityPlayspaceBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayspaceBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.hide()

        setupPlayer()
    }

    private fun setupPlayer() {

        /*
         Player init
         Optionally, you can also define PlayspaceSettings and PlayspaceCustomization objects here
         */
        val appSettings = AppSettings(
            domainURL = "https://example.com",
            storeURL = "https://playstore.com/example",
            appCategories = listOf("news"),
            hasPrivacyPolicy = true,
            isPaid = false,
            appPageURL = "https://example.com/web-equivalent-article-page"
        )
        val customization = PlayspaceCustomization(
            logoUrl = null,
            orientation = StoryOrientation.LANDSCAPE
        )
        val playerSettings = PlayspaceSettings(
            playbackMode = PlaybackMode.AUTO_PLAY,
            defaultSoundMode = DefaultSoundMode.ON,
            customization,
            null,
            null,
            null,
            null,
            null,
            null
        )
        val playspaceConfig = PlayspaceConfig(
            playerId = "01eadd01-38d3-4143-ac4a-22a37eb2c1e8",
            customerId = "51a004ea-0018-4342-83c4-9a983a0ee702",
            appSettings = appSettings,
            settings = playerSettings,
        )

        connatixPlayer = PlayspacePlayer(this, playspaceConfig, this)

        // Add player event listeners
        connatixPlayer.listenFor(EventType.PAUSE)
        connatixPlayer.listenFor(EventType.PLAY)
        connatixPlayer.listenFor(EventType.VOLUME_CHANGED)

        // Or listen to all events
        connatixPlayer.listenForAllEvents()

        // Add player to layout
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        connatixPlayer.layoutParams = params
        binding.playspaceContainer.addView(connatixPlayer.view)

        // Start rendering the player
        connatixPlayer.start()
    }

    override fun onPause() {
        connatixPlayer.prepareForOnPause()
        super.onPause()
    }

    override fun onResume() {
        connatixPlayer.prepareForOnResume()
        super.onResume()
    }

    override fun onDestroy() {
        connatixPlayer.stop()
        super.onDestroy()
    }

    override fun receivedConnatixEvent(event: PlayerEvent) {
        Log.d(TAG, "🔴 Event type: ${event.type.value}, payload: ${event.payload}")
    }
}