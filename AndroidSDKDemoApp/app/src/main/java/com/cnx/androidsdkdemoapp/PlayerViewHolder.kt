package com.cnx.androidsdkdemoapp

import android.content.Context
import android.util.Log
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.cnx.androidsdkdemoapp.databinding.ListConnatixPlayerCellBinding
import com.cnx.connatixplayersdk.external.ElementsPlayer
import com.cnx.connatixplayersdk.external.interfaces.ConnatixPlayerListener
import com.cnx.connatixplayersdk.external.models.*

private const val TAG = "PlayerViewHolder"

class PlayerViewHolder(private val binding: ListConnatixPlayerCellBinding,
                       private val context: Context) :
    RecyclerView.ViewHolder(binding.root), ConnatixPlayerListener {

    private lateinit var connatixPlayer: ElementsPlayer

    fun bind() {

        /*
        Player init
        Optionally, you can also define ElementsSettings and ElementsCustomization objects here
        */
        val appSettings = AppSettings(
            domainURL = "https://example.com",
            storeURL = "https://playstore.com/example",
            appCategories = listOf("news"),
            hasPrivacyPolicy = true,
            isPaid = false,
            appPageURL = "https://example.com/web-equivalent-article-page",
            useContentOnlyDomain = true
        )

        val elementsConfig = ElementsConfig(
            playerId = "a1c25364-9ad9-4636-a194-7886a24aedf7",
            customerId = "51a004ea-0018-4342-83c4-9a983a0ee702",
            appSettings = appSettings
        )

        connatixPlayer = ElementsPlayer(context, elementsConfig, this)

        // Add player event listeners
        connatixPlayer.listenFor(EventType.PAUSE)
        connatixPlayer.listenFor(EventType.PLAY)
        connatixPlayer.listenFor(EventType.VOLUME_CHANGED)

        // Or listen to all events
        connatixPlayer.listenForAllEvents()

        // Start rendering the player
        connatixPlayer.start()

        // Add player to layout
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        connatixPlayer.layoutParams = params
        binding.listElementsContainer.addView(connatixPlayer.view)
    }

    override fun receivedConnatixEvent(event: PlayerEvent) {
        Log.d(TAG, "🔥 Event type: ${event.type.value}, payload: ${event.payload}")
    }
}