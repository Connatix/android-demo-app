package com.cnx.androidsdkdemoapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cnx.androidsdkdemoapp.databinding.ListConnatixPlayerCellBinding
import com.cnx.androidsdkdemoapp.databinding.ListPlaceholderCellBinding

private const val TYPE_IMAGE = 1
private const val TYPE_PLAYER = 2

class RecyclerViewAdapter(private val imageList: Array<Int?>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return imageList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 6) TYPE_PLAYER else TYPE_IMAGE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == TYPE_PLAYER) {
            val binding = ListConnatixPlayerCellBinding.inflate(LayoutInflater.from(parent.context))
            PlayerViewHolder(binding, parent.context)
        } else {
            val binding = ListPlaceholderCellBinding.inflate(LayoutInflater.from(parent.context))
            ImageViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (getItemViewType(position) == TYPE_PLAYER
            && holder is PlayerViewHolder) {
            holder.bind()
        } else if (holder is ImageViewHolder) {
            holder.bind()
        }
    }
}