package com.cnx.androidsdkdemoapp

import android.app.Application
import com.google.android.gms.ads.MobileAds

class DemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        MobileAds.initialize(this)
    }
}